#include "Graphics.h"

//Default colors
const SDL_Color colorBlack = {0, 0, 0, 0};
const SDL_Color colorWhite = {0xff, 0xff, 0xff, 0};
const SDL_Color colorRed = {0xff, 0x0, 0x0, 0};
const SDL_Color colorGreen = {0x0, 0xff, 0x0, 0};
const SDL_Color colorBlue = {0x0, 0x0, 0xff, 0};
const SDL_Color colorCyan = {0x0, 0xff, 0xff, 0};
const SDL_Color colorMagenta = {0xff, 0x0, 0xff, 0};
const SDL_Color colorYellow = {0xff, 0x0, 0xff, 0};

//Startup / Cleanup
SDL_Surface* initializeScreen(int width, int height, int bpp, BOOL fullscreen)
{
	SDL_Surface *screen = NULL;	
	
	screen = SDL_SetVideoMode(width, height, bpp, SDL_HWSURFACE | SDL_DOUBLEBUF | (SDL_FULLSCREEN & fullscreen));
	if(!screen) printf("Error creating primary screen in SDL\n");



	return screen;
}

BOOL initializeSDL()
{
	BOOL retVal = FALSE;
	
	if(SDL_Init(SDL_INIT_EVERYTHING))
	{
		printf("Error initializating SDL_INIT_EVERYTHING\n");
		return FALSE;
	}

	if(!IMG_Init(IMG_INIT_PNG)) 
	{
		printf("Error initializing SDL_Image\n");
		return FALSE;
	}

	if (TTF_Init() == -1)
	{
		printf("Error initializing SDL_TTF\n");
		return FALSE;
	}

	if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 4096) == -1)
	{
		printf("Error initializing SDL_mixer\n");
		return FALSE;
	}

	FontLoadFonts();
	return retVal;
}

void cleanGraphics(SDL_Surface* screen)
{
	SDL_FreeSurface(screen);
	screen = NULL;
	FontUnloadFonts();
	TTF_Quit();
	IMG_Quit();
	SDL_Quit();
	Mix_CloseAudio();
}

//Images
SDL_Surface* loadPngAlphaImage(char *path, BOOL alpha, SDL_Rect *clipRect)
{
	SDL_Surface *rawImage = NULL;
	SDL_Surface *optimizedImage = NULL;

	if(!(rawImage = IMG_Load(path)))
	{
		printf("Error loading image: %s\n", path);
		return NULL;
	}
	if (clipRect) SDL_SetClipRect(rawImage, clipRect);

	//if (alpha)
	//	optimizedImage = SDL_DisplayFormatAlpha(rawImage);
	//else
	//	optimizedImage = SDL_DisplayFormat(rawImage);

	//SDL_FreeSurface(rawImage);
	//return optimizedImage;

	return rawImage;
}

SDL_Surface* loadBmpColorKey(char *path, Uint32 colorKey)
{
	SDL_Surface *rawImage = NULL;
	SDL_Surface *optimizedImage = NULL;

	rawImage = SDL_LoadBMP(path);
	SDL_SetColorKey(rawImage, SDL_SRCCOLORKEY, colorKey);
	optimizedImage = SDL_DisplayFormatAlpha(rawImage);


	SDL_FreeSurface(rawImage);
	rawImage = NULL;

	return optimizedImage;
}

//Surfaces
BOOL applySurfaceRect(SDL_Surface* target, SDL_Surface* source, int posX, int posY, SDL_Rect sourceClip)
{
	SDL_Rect targetPosRect;
	if(!(target && source)) return FALSE;

	targetPosRect.x = posX;
	targetPosRect.y = posY;

	return SDL_BlitSurface(source, &sourceClip, target, &targetPosRect) == 0;
}

BOOL applySurfaceClip(SDL_Surface* target, SDL_Surface* source, int posX, int posY, int sX, int sY, int sW, int sH)
{
	SDL_Rect sourceRect;

	sourceRect.x = sX;
	sourceRect.y = sY;
	sourceRect.w = sW;
	sourceRect.h = sH;

	if(!(target && source)) return FALSE;

	return applySurfaceRect(target, source, posX, posY, sourceRect);
}

BOOL applySurface(SDL_Surface* target, SDL_Surface* source, int posX, int posY)
{
	SDL_Rect targetPosRect;
	if(!(target && source)) return FALSE;

	targetPosRect.x = posX;
	targetPosRect.y = posY;
	
	return SDL_BlitSurface(source, NULL, target, &targetPosRect) == 0;
}

//Shorts
SDL_Color getColor(Uint8 r, Uint8 g, Uint8 b)
{
	SDL_Color tmpColor = {r, g, b, 0};
	return tmpColor;
}

typedef union _color
{
	SDL_Color sdlColor;
	Uint32 color32;
} color32Union;

SDL_Color getColor32(Uint32 color)
{
	color32Union tmpColor;
	tmpColor.color32 = color;

	return tmpColor.sdlColor;
}