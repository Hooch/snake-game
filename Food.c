#include "Food.h"

CREATE_LIST_TYPE_C(Food);

int FoodPower[FOOD_TYPE_COUNT];
Sprite* FoodTypeSprite[FOOD_TYPE_COUNT];

void FoodInitialize()
{
	FoodPower[FOOD_TYPE_NORMAL] = 1;
}

void FoodManagerRelease(FoodManager* that)
{
	if(that->foodList) Food_list_releaseMemory(that->foodList);
	FREE(that);
}

void FoodManagerAddFood(FoodManager* that, Vec2d pos, FOOD_TYPE type)
{
	Food_listElement *tempFoodElem;
	Food food;

	food.creationTime = SDL_GetTicks() / 1000.0f;
	food.power = FoodPower[type];
	food.type = type;
	food.position = pos;

	tempFoodElem = Food_list_getNewListElement();
	tempFoodElem->value = food;

	if(that->foodList)
	{
		Food_list_appendToList(that->foodList, tempFoodElem);
	} else
	{
		that->foodList = tempFoodElem;
	}
}

void FoodManagerUpdate(FoodManager* that, Snake* snake, timeSec dTime, Vec2d boardSize)
{
	Vec2d headPos, temp;
	Food_listElement* foodElem, *tempFoodElem;
	SnakeSegment_listElement *snakeSegment;
	int i;
	BOOL freePos = FALSE;
	Uint32 startTime, allTime;

	headPos = snake->snakeSegmentsList->value.pos;
	for(foodElem = that->foodList; foodElem != NULL;)
	{
		if(Vec2dEqual(foodElem->value.position, headPos))
		{
			Mix_PlayChannel(-1, snake->eatSound, 0);
			snake->foodInGut += FoodPower[foodElem->value.type];
			snake->foodPowerEaten += FoodPower[foodElem->value.type];
			tempFoodElem = foodElem;

			if(Food_list_getElementCount(foodElem) == 1)
			{
				that->foodList = NULL;
			}
			foodElem = foodElem->next;

			Food_list_unlinkFromList(tempFoodElem);
			FREE(tempFoodElem);			
		} else
		{
			foodElem = foodElem->next;
		}
	}

	if(Food_list_getElementCount(that->foodList) < that->maxFoodAnOnce)
	{
		startTime = SDL_GetTicks();

		temp = Vec2dInit(0, 0);
		for(i = 0; i < that->triesPerFrame; i++)
		{
			freePos = TRUE;
			temp.x = rand() % boardSize.x;
			temp.y = rand() % boardSize.y;
							
			for(snakeSegment = snake->snakeSegmentsList; snakeSegment; snakeSegment = snakeSegment->next)
			{
				if(Vec2dEqual(snakeSegment->value.pos, temp)) {freePos = FALSE; break;}			
			}

			if(freePos)
			{
				FoodManagerAddFood(that, temp, FOOD_TYPE_NORMAL);
				break;
			}
		}
		Sleep(10);
		
		allTime = SDL_GetTicks() - startTime;

		//Jezeli nie da sie znalesc latwo miejsca na zarcie
		if(i >= that->triesPerFrame && allTime)
		{
#ifdef _DEBUG
			printf("Max tries (%d) used in %d ms.\n", i, allTime);
#endif
			that->triesPerFrame = ((16 * that->triesPerFrame) / allTime) + 1;
#ifdef _DEBUG
			printf("New tries/frame: %d\n", that->triesPerFrame);
#endif
		} else
		//Jezelie szukanie zajmuje za duzo czasu
		if(allTime > 16)
		{
#ifdef _DEBUG
			printf("Lag of %d ms with %d tries of %d.\n", allTime, i, that->triesPerFrame);
#endif
			that->triesPerFrame = ((16 * i) / allTime) + 1;
#ifdef _DEBUG
			printf("New tries/frame: %d\n", that->triesPerFrame);
#endif
		}	
	}
}

void FoodManagerRender(FoodManager* that, SDL_Surface *surface, int cellSize)
{
	Food_listElement *tempFoodElem = NULL;

	for(tempFoodElem = that->foodList; tempFoodElem != NULL; tempFoodElem = tempFoodElem->next)
	{
		SpriteApplyToSurface(FoodTypeSprite[tempFoodElem->value.type], surface, Vec2dRetrunMultipliedByScalar(&(tempFoodElem->value.position), cellSize));
	}
}