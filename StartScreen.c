#include "StartScreen.h"

BOOL StartScreenHandleInput(StartScreen* that, SDL_Event *sdlEvent, timeSec dTime)
{
	if(sdlEvent->type == SDL_KEYDOWN)
	{
		switch (sdlEvent->key.keysym.sym)
		{
		case SDLK_SPACE:
			that->readyToContinue = TRUE;
			return TRUE;
			break;
		default:
			break;
		}
	}
	return FALSE;
}

void StartScreenRender(StartScreen* that, SDL_Surface* surface, timeSec dTime)
{
	SDL_Rect msgSize;

	msgSize = FontRenderToSurface(surface, surface->w / 2, surface->h / 2, TEXT_ALIGN_CENTER, TEXT_ALIGN_MIDDLE, Fonts[FONT_H1], colorBlack, colorWhite, FONT_Q_BLENDED, "SNAKE");
	FontRenderToSurface(surface, surface->w / 2, msgSize.y + msgSize.h + 10, TEXT_ALIGN_CENTER, TEXT_ALIGN_MIDDLE, Fonts[FONT_H4], colorBlack, colorWhite, FONT_Q_BLENDED, "Press [Space] to start");

	msgSize = FontRenderToSurface(surface, surface->w / 2, surface->h - 10, TEXT_ALIGN_CENTER, TEXT_ALIGN_BOTTOM, Fonts[FONT_SMALL], colorBlack, colorWhite, FONT_Q_BLENDED, "Game made by Maciej Kusnierz - 2013");
}