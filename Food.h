#pragma once
#include "Graphics.h"
#include "ListTemplate.h"
#include "Types.h"
#include "Snake.h"
#include "UsefullMacros.h"

typedef struct SnakeT Snake;//Potrzebne bo Snake.h i Food.h importuj� si� wzajemnie

//Rodzaje jedzenia.
//W tej chwili jest tylko jeden rodzaj, ale dzi�ki temu rozwi�zaniu dodanie nowego typu jedzenia to kwestia kilku linijek kodu.
typedef enum
{
	FOOD_TYPE_NORMAL = 0,
	FOOD_TYPE_COUNT
} FOOD_TYPE;

//Tablica z prerenderowanymi spritami jedzenia.
//S� generowane przy starcie programu i potem tylko wy�wietlane.
//Znacz�co przyspiesza renderowanie klatki.
extern Sprite* FoodTypeSprite[FOOD_TYPE_COUNT];

//Struktura opisuj�ca jedzenie.
//Pozycje, typ, moc oraz czas stworzenia.
typedef struct FoodT
{
	Vec2d position;
	int power;
	FOOD_TYPE type;
	timeSec creationTime;
} Food;

//Deklaracja funkcji listy przechowuj�cej jedzenie
CREATE_LIST_TYPE_H(Food)

//Menad�er zajmuj�cy si� kwesti� wy�ywienia.
typedef struct FoodManagerT
{
	int maxFoodAnOnce;
	int minFoodAtOnce;
	Food_listElement* foodList;
	int triesPerFrame;
} FoodManager;

//Tablica mocy jedzenia.
//Jest globalna po to aby nie trzeba by�o przesy�a� jej wsz�dzie (du�y spadek wydajno�ci)
extern int FoodPower[FOOD_TYPE_COUNT];

//Inicjalizacja Menad�era jedzenia.
void FoodInitialize();
//Zwolninie wszystkich zasob�w u�ytych przez menad�era.
void FoodManagerRelease(FoodManager* that);

//F-cja wywo�ywana na pocz�tku ka�dej klati. Zajmuje si� jedzeniem.
//Monitorowaie jedzeia, tworzenie nowego.
void FoodManagerUpdate(FoodManager* that, Snake* snake, timeSec dTime, Vec2d boardSize);

//Funkja dodaj�ca jedzenie zadanego typu na dane pole.
void FoodManagerAddFood(FoodManager* that, Vec2d pos, FOOD_TYPE type);
//F-cja renderuj�ca jedzenie.
void FoodManagerRender(FoodManager* that, SDL_Surface *surface, int cellSize);