#pragma once
//G��wne ustawienia programu w czsie kompilacji

#define SCREEN_BPP		32
#define SCREEN_FULLSCREEN 0

//Foldery plik�w
#define IMAGES_FOLDER "Images\\"
#define FONTS_FOLDER "Fonts\\"
#define SOUNDS_FOLDER "Sounds\\"

#define SAVE_FILE_NAME "score.snake"

//Dolny pasek z wynikiem
#define BOTTOM_BAR_HEIGHT 32
#define BOTTOM_BAR_BG 0x000000
#define BOTTOM_BAR_FG 0x99cc99

//Kolory
//0xrrggbbaa
#define BG_COLOR 0x99cc99
#define SNAKE_COLOR 0x000000ff
#define BG_PIXEL 0x00000008
#define FOOD_1_COLOR 0x000000ff

#define SNAKE_STARTING_FOOD 2
#define MAX_FOOD_ON_SCREEN 1

#define FOOD_TRIES_PER_FRAME 25
