#pragma once
#include "UsefullMacros.h"
#include <Windows.h>
#include "ListTemplate.h"

//Bardzo prosta klasa do obs�ugi wekto�w 2D

typedef struct
{
	int x, y;
} Vec2d;

//Deklaracja listy wektor�w.
CREATE_LIST_TYPE_H(Vec2d)

//Zwraca wektor z danymi warto�ciami x i y.
Vec2d Vec2dInit(int x, int y);

//Dodaje do wektroa skalar.
Vec2d* Vec2dAddScalar(Vec2d* that, int arg);

//Sprawdza czy 2 wektory s� r�wne. Maj� takie same wsp��dne.
BOOL Vec2dEqual(Vec2d a, Vec2d b);

//Pomno�enie wektroa przez skalar.
Vec2d* Vec2dMultiplyByScalar(Vec2d* that, int arg);

//Zwraca wektor pomno�ony przez skalar.
//Nie zmienia warto�ci w oryginalnym wektorze przes�anym do f-cji.
Vec2d Vec2dRetrunMultipliedByScalar(const Vec2d* a, int arg);