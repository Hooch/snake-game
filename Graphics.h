#pragma once
#pragma comment(lib, "SDL_ttf.lib")
#pragma comment(lib, "SDLmain.lib")
#pragma comment(lib, "SDL.lib")
#pragma comment(lib, "SDL_image.lib")
#pragma comment(lib, "SDL_gfx.lib")
#pragma comment(lib, "SDL_mixer.lib")

#include <Windows.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_framerate.h>
#include <SDL_gfxPrimitives.h>
#include <SDL_mixer.h>
#include "Types.h"
#include "Sprite.h"
#include "Vec2d.h"
#include "Text.h"

//M�j ma�y wrapper na fukcje biblioteki SDL u�atwiaj�cy jej u�ywanie.

#define SDL_RGB(r, g, b) (((r & 0xff) << 16) | ((g & 0xff) << 8) | (b & 0xff))

//Startup / Cleanup
//F-cje inicjalizuj�ce grafik� oraz zwalniaj�ce zasoby.
SDL_Surface* initializeScreen(int width, int height, int bpp, BOOL fullscreen);
BOOL initializeSDL();
void cleanGraphics(SDL_Surface* screen);

//Images
//F-cje �aduj�ce grafike z plik�w.
SDL_Surface* loadPngAlphaImage(char *path, BOOL alpha, SDL_Rect *clipRect);
SDL_Surface* loadBmpColorKey(char *path, Uint32 colorKey);

//Surfaces
//F-cje zajmuj�ce si� kopiowaniem warst mi�dzy sob�.
BOOL applySurfaceRect(SDL_Surface* target, SDL_Surface* source, int posX, int posY, SDL_Rect sourceClip);
BOOL applySurfaceClip(SDL_Surface* target, SDL_Surface* source, int posX, int posY, int sX, int sY, int sW, int sH);
BOOL applySurface(SDL_Surface* target, SDL_Surface* source, int posX, int posY);

//Shorts
//F-cje skracaj�ce proste czynno�ci.
SDL_Color getColor(Uint8 r, Uint8 g, Uint8 b);
SDL_Color getColor32(Uint32 color);

//Default colors
//Kilka podstawowych kolor�w.
//G��wnie u�ywanie przy debugowaniu.
extern const SDL_Color colorBlack;
extern const SDL_Color colorWhite;
extern const SDL_Color colorRed;
extern const SDL_Color colorGreen;
extern const SDL_Color colorBlue;
extern const SDL_Color colorCyan;
extern const SDL_Color colorMagenta;
extern const SDL_Color colorYellow;