#pragma once
#include "Graphics.h"

//Scena ko�cowa.
//Jest wy�wietlana przy zabiciu si�.
//Wy�wietla wynik gracza oraz najwyrzszy wynik do pobicia.
typedef struct
{
	BOOL readyToContinue;
	scoreT playerScore;
	scoreT highscore;
} EndScreen;

//Funkja zajmuj�ca si� obs�ug� klawiatury w ko�cowej scenie.
BOOL EndScreenHandleInput(EndScreen* that, SDL_Event *sdlEvent, timeSec dTime);
//Funkja zajmuj�ca si� renderem sceny.
void EndScreenRender(EndScreen* that, SDL_Surface* surface, timeSec dTime);