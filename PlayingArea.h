#pragma once
#include <string.h>
#include "Snake.h"
#include "Graphics.h"
#include "Defines.h"
#include "StartScreen.h"
#include "EndScreen.h"
#include "Food.h"

/*
M�zg programu.
PlayingArea jest menad�erem zajmuj�cym si� rogrywk�, zasadmi gry oraz scenami pomocniczymi.
PlayingArea zajmuje si� poprawnym rozes�aniem sygna��w od gracza.
Zajmuje si� poprawnym renderowaniem scen, liczeniem wyniku,
rozszyfrowaniem argument�w startowych.
*/
extern Sprite* snakeSegmentStateSprite[SEGMENT_TYPE_COUNT];

//Stan gry
typedef enum
{
	PLAYING_PLAYING = 0,
	PLAYING_START_SCREEN,
	PLAYING_END
} PLAYING_STATE;

typedef struct
{
	Snake* snake;
	Vec2d boardSize;
	int cell_size;
	PLAYING_STATE state;
	timeSec startTime;
	timeSec lastInputHandled;
	BOOL borders;
	StartScreen* startScreen;
	EndScreen* endScreen;
	FoodManager* foodManager;
	scoreT score;
	scoreT highscore;
	Sprite* boardBackground;
	Mix_Music* mainThemeMusic;
} PlayingArea;

//Tworzy gr� z argument�w startowych
PlayingArea* PlayingAreaGetFromInputArgs(int argc, char **argv);

//Zwraca ca�kowity rozmiar okna gry w pixelach. U�ywane przy inicjalizowaniu grafiki.
Vec2d PlayingAreaGetTotalSize(PlayingArea *that);

//Zwolnienie wszystkich zasob�w u�ywanich przez gr�.
void PlayingAreaRelease(PlayingArea* that);

//Rozsy�a input gracza do odpowiednich "scen" i podsystem�w/menad�er�w.
void PlayingAreaHandleEvent(PlayingArea* that, SDL_Event* sdlEvent, timeSec dTime);

//Oblcza stan gry na pocz�tku ka�dej klatki. Ka�e wszytkich aktywym mena�erom wykona� "update" po swojej stronie.
//Tworzenie jedzenia, przes�wanie snake, liczenie wyniku, sprawdzanie kolizji, kompensacja lag�w.
void PlayingAreaUpdate(PlayingArea* that, timeSec dTime);

//Renderuje scene wy�ietlan� na ekranie. W zale�no�ci od statu gry wywoluje funkcje renderuj�ce w r�nych podsystemach.
void PlayingAreaRender(PlayingArea* that, SDL_Surface* surface, timeSec dTime);

//Renderuje dolny pasek z wynikiem
void PlayingAreaRenderBottomBar(PlayingArea* that, SDL_Surface* surface);

//Generowanie dynamicznych tekst�r
//�adowanie gry.
void GenerateSnakeSegmentStateSprites(int size);
void GenerateFoodTypeSprites(int size);

//Rozpoczyna now� gr�.
void PlayingAreaStartGame(PlayingArea* that);

//Czytanie i zapisywanie najwy�szego wyniku do pliku.
void PlayingAreaReadHighscore(PlayingArea* that,char* filename);
void PlayingAreaSaveHighscore(PlayingArea* that,char* filename);