#include "Sprite.h"

Sprite* SpriteGetFromFile(char* path, BOOL usesAlpha, SDL_Rect* trimRect)
{
	Sprite* sprite = NEW(Sprite);

	sprite->surface = loadPngAlphaImage(path, usesAlpha, trimRect);
	
	if(sprite->surface)
		return sprite;
	else
		return NULL;
}

BOOL SpriteApplyToSurface(Sprite* that, SDL_Surface* target, Vec2d pos)
{
	return applySurface(target, that->surface, pos.x, pos.y);
}

BOOL SpriteApplyToSurfaceWithClip(Sprite* that, SDL_Surface* target, int x, int y, int sX, int sY, int sW, int sH)
{
	return applySurfaceClip(target, that->surface, x, y, sX, sY, sW, sH);
}

void SpriteRelease(Sprite* that)
{
	if(that->surface)
	{
		SDL_FreeSurface(that->surface);
		that->surface = NULL;
	}
	FREE(that);
}

Sprite* SpriteWrapSurface(SDL_Surface* surface)
{
	Sprite* sprite;
	sprite = NEW(Sprite);
	sprite->surface = surface;
	surface->refcount++;
	sprite->w = surface->w;
	sprite->h = surface->h;
	return sprite;
}